/*
	Naming convention for controllers is that it should be named after the model/documents it is concerned with.
*/
//The controllers are functions which contain the actual business logic of our API and is triggered by a route.

//MVC - models, views and controller

//TO create a controller, we first add it into our module.exports
//So that we can import the controllers from our module

//import the User model in the controllers instead because this is where we are now going to use it.
const User = require("../models/User");
//import course
const Course = require("../models/Course");
//import bcrypt
//bcrypt is a package which allows us to hash our passwords to add a layer of security for our's users' details
const bcrypt = require("bcrypt");
//import auth.js module to createAccessToken and its subsequent methods
const auth = require("../auth");


module.exports.registerUser = (req,res)=>{


	//console.log(req.body)//check the input passed via the client

	//Using bcrypt, we're going to hide the user's password underneath a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash
	//bcrypt.hashSync(<string>,<saltRounds>)
	const hashedPw = bcrypt.hashSync(req.body.password,10);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.getUserDetails = (req,res)=>{

	//find() will return an array of documents which matches the criteria.
	//It will return an array even if it only found 1 document
	//findOne()
	//findById()
	let userId = req.user.id;

	User.findOne({_id:userId})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res)=>{

	/*
		Steps for logging in our user:

		1. Find the user by its email
		2. If we find the user, we will check his password if the password input and the hashed password in our db matches.
		3. If upon checking the found user's password is the same as the input password, we will generate a "key" for the user to have authorization to access certain features in our app.
	*/
	//mongodb: db.users.findOne({email:})
	User.findOne({email:req.body.email})
	.then(foundUser => {

		//foundUser is the parameter that contains the result of findOne
		//findOne() returns null if it is not able to match any document
		if (foundUser === null){
			return res.send({message: "No User Found."})
		} else {
			// console.log(foundUser);
			//if we find a user, foundUser will contain the document that matched the input email.
			//check if the input password from req.body matches the hashed password in our foundUser document
			/*
				bcrypt.compareSync(<inputString>,<hashedString>)
			*/
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
			// console.log(isPasswordCorrect);

			//if the password is correct, we will create a "key", a token for our user, else, we will send a message:
			if(isPasswordCorrect){

				/*
					To be able to create a "key" or token that allows/authorizes our logged in user around our application, we have to create our own module called auth.js.

					This module will create a encoded string which contains our user's details.

					This encoded string is what we call a JSONWebToken or JWT.

					This JWT can only be properly unwrapped or decoded with our own secret word/string.
				*/

				// console.log("We will create a token for the user if the password is correct")

				//auth.createAccessToken receives our foundUser document as an argument, gets only necessary details, wraps those details in JWT and our secret, then finally returns our JWT. This JWT will be sent to our client
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send({message: "Incorrect Password"});
			}
		}
	})

}

//checks if email exists or not
module.exports.checkEmail = (req,res) =>{

	User.findOne({email:req.body.email})
	.then(result => {

		//findOne will return null if no match is found
		if (result === null){
			return res.send(false)
		} else {
			return res.send(true)
		}

	})
	.catch(error => res.send(error))

}

module.exports.enroll = async (req,res) => {

	//check the id of the user who will enroll
	// console.log(req.user.id);

	//check the id of the course we want to enroll
	// console.log(req.body.courseId)

	//validate the user if they are an admin or not
	//if the user is an admin, send a message to client and end the response
	//else, we will continue
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	}

	/*
		Enrollment will come in 2 steps

		First, find the user who is enrolling and update his enrollments subdocument array. We will push the courseId in the enrollments array.
		Second, find the course where we are enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.

		Since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting Javascript continue line per line.

		async and await - async keyword is added to a function to make our function asynchronous. Which means that instead of JS regular behavior of running each code line by line we will be able to wait for the result of a function.

		To be able to wait for the result of a function, we use the await keyword. They await keyword allows us to wait for the function to finish and get a result before proceeding.
	*/

	//return a boolean to our isUserUpdated variable to determine the result of the query and if we were able to save the courseId into our user's enrollment subdocument array
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		
		// console.log(user.enrollments.includes(req.body.courseId))

		//add the courseId in an object and push that object into the user's enrollments.
		///because we have to follow the schema of the enrollments subdocument array

		let newEnrollment = {
			courseId: req.body.courseId
		}

		// if (user.enrollments.includes({"courseId":req.body.courseId})){
		// 	console.log(TRUE)
		// 	// return res.send({message: "Already Enrolled"});
		// }


		//access the enrollment array from our user and push the new enrollment subdocument into the enrollments array.
		user.enrollments.push(newEnrollment)

		//we must save the user document and return the value of saving our document
		//return true if we push the subdocument successfully
		//catch and return error message if otherwise/

		return user.save().then(user=>true).catch(err => err.message)


	})
	//if user was able to enroll properly, isUserUpdated contains true.
	//else, isUserUpdated will contain an error message
	// console.log(isUserUpdated);

	//add an if statement and stop the process if isUserUpdated does not contain true
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}

	//find the course where we will enroll or add the user as an enrollee and return true if we are able to push the user into the enrollees array properly or send the error message instead
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		// console.log(course);

		//create an object
		let enrollee = {
			userId: req.user.id
		}

		//push the enrollee into the enrollees subdocument array of the course:
		course.enrollees.push(enrollee);

		//save the course document
		//return true if we were able to save and add the user as enrollee properly
		//return an err message if we catch an error
		return course.save().then(course => true).catch(err=>err.message);


	})

	// console.log(isCourseUpdated);
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Thank you for enrolling"})
	}
}