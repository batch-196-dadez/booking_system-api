//Import the Course model so we can manipulate it and add a new course document.
const Course = require("../models/Course");

module.exports.getAllCourses = (req,res)=>{

	//Use the Course model to connect to our collection and retrieve our courses.
	//to be able to query into our collections we use the model connected to that collection
	//in mongodb: db.courses.find({})
	//Model.find() - returns a collection of documents that matches our criteria similar to mongodb's .find()
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.addCourse = (req,res)=>{

	//console.log(req.body);
	//Using the Course model, we will use its constructor to create our Course document which will follow the schema of the model, and add methods for document creation.

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	//newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor
	//.save() method is added into our newCourse object. this will allow us to save the content of newCourse into our collection

	//.then() allows us to process the result of a previous function/method in its own anonymous function

	//.catch() - catches the errors and allows us to process and send to the client

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.getSingleCourse = (req,res) => {

	// console.log(req.params);
	//req.params is an object that contains the value captured via route params
	//the field name of the req.params indicate the name of the route params

	Course.findOne({_id:req.params.courseId})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.updateCourse = (req,res) => {

	//findByIdAndUpdate - used to update documents and has 3 arguments
	//findByIdAndUpdate(<id>,{updates},{new:true})

	//We can create a new object to filter update details
	//The indicated fields in the update object will be the fields updated.
	//fields/property that are not part of the update object will not be updated

	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.archiveCourse = (req,res) => {

	let update = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}